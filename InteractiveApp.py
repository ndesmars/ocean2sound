# -*- coding: utf-8 -*-
'''
For more details than those found in the documentation, code comments and
references, please let me know at:
    nicolas _dot_ desmars _at_ protonmail _dot_ com

License
----------
Copyright 2024 Nicolas Desmars

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

import pyaudio
from CustomColorMaps import CustomColorMaps
from numpy import stack, vstack, hstack, log10, array, int16,  moveaxis, \
    concatenate, zeros
from PyQt5.QtWidgets import QLabel, QSizePolicy, QSlider, QSpacerItem, \
    QVBoxLayout, QWidget, QGridLayout, QComboBox
from pyqtgraph import Qt, opengl, ColorMap, Vector
from queue import Queue
import sys


class ComboBox(QWidget):
    '''
    Class that defines the Qt comboboxes that are used in the GUI.
    '''

    def __init__(self, string_list, p_full_name, default_kw, **kwargs):
        '''
        Constructor.

        Inputs
        ----------
        string_list :: list :: List of strings to be displayed by the combobox
                               when selecting the item.
        p_full_name :: string :: Full name of the combobox's list of items.
        default_kw :: string :: Key word of the combobox's item that is
                                selected by default.

        kwargs
        ----------
        kw_list :: list :: List of key words (strings) referring to the
                           combobox's items.
        '''
        super(ComboBox, self).__init__(parent=None)
        # get the optional key-word list
        self.kw_list = kwargs.get('kw_list', string_list)
        # create vertical layout
        verticalLayout = QVBoxLayout(self)
        # add space on the top of the label
        spacerItemL = QSpacerItem(0, -5, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemL)
        # create and add a label
        label = QLabel(self)
        verticalLayout.addWidget(label)
        label.setText(p_full_name)
        label.setAlignment(Qt.QtCore.Qt.AlignCenter)
        # add space on the top of the combobox
        spacerItemL = QSpacerItem(0, -5, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemL)
        # create and add combobox
        self.combobox = QComboBox(self)
        self.combobox.addItems(string_list)
        verticalLayout.addWidget(self.combobox)
        # add space on the bottom of the combobox
        spacerItemL = QSpacerItem(0, -5, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemL)
        # get name of the default item
        default_text = string_list[self.kw_list.index(default_kw)]
        # set default value of the combobox
        index = self.combobox.findText(default_text,
                                       Qt.QtCore.Qt.MatchFixedString)
        self.combobox.setCurrentIndex(index)
        # init state (value modified by the user or not) to False
        self.state = False


class Slider(QWidget):
    '''
    Class that defines the Qt sliders that are used in the GUI.
    '''

    def __init__(self, p_name, p_full_name, default_x, minimum, maximum,
                 i_multiple=False):
        '''
        Constructor.

        Inputs
        ----------
        p_name :: string :: Short name of the slider's parameter.
        p_full_name :: string :: Full name of the slider's parameter.
        default_x :: float :: Default value the slider's parameter.
        minimum :: float :: Minimum value the slider's parameter.
        maximum :: float :: Maximum value the slider's parameter.
        i_multiple :: bool :: If True, the domain is replicated multiple times.
        '''
        super(Slider, self).__init__(parent=None)
        self.i_multiple = i_multiple
        # create vertical layout
        verticalLayout = QVBoxLayout(self)
        # add space on the top of the label
        spacerItemL = QSpacerItem(0, -10, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemL)
        # create and add label (slider value)
        self.label = QLabel(self)
        verticalLayout.addWidget(self.label)
        # add space on the top of the slider
        spacerItemL = QSpacerItem(0, -10, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemL)
        # create and add the slider
        self.slider = QSlider(self)
        self.slider.setOrientation(Qt.QtCore.Qt.Horizontal)
        # add space on the bottom
        verticalLayout.addWidget(self.slider)
        spacerItemR = QSpacerItem(0, -10, QSizePolicy.Minimum,
                                  QSizePolicy.Minimum)
        verticalLayout.addItem(spacerItemR)
        # adapt label value when the value is modified
        self.p_full_name = p_full_name
        self.minimum = minimum
        self.maximum = maximum
        if p_name in {'Tp', 'gamma', 'eps', 's', 'NL', 'mu'}:
            # to have a logarithmical evolution
            self.i_log = True
        else:
            # to have a linear evolution
            self.i_log = False
        v = self.get_value_from_x(default_x)
        self.slider.setValue(v)
        self.slider.valueChanged.connect(self.set_label_value)
        self.set_label_value(self.slider.value())
        # init state (value modified by the user or not) to False
        self.state = False

    def get_value_from_x(self, x):
        '''
        Method that gets the value (from 0 to 100 %) of the slider from the
        input (dimensional value of the parameter).

        Inputs
        ----------
        x :: float :: dimensional value of the slider's parameter.

        Outputs
        ----------
        value :: int :: value in percent of the slider's parameter.
        '''
        if self.i_log:
            # to have a logarithmical evolution
            value = int(self.slider.minimum() +
                        ((log10(x)-log10(self.minimum)) /
                         (log10(self.maximum)-log10(self.minimum))
                         )*(self.slider.maximum()-self.slider.minimum()))
        else:
            # to have a linear evolution
            value = int(self.slider.minimum() +
                        ((x-self.minimum) /
                         (self.maximum-self.minimum)
                         )*(self.slider.maximum()-self.slider.minimum()))
        return value

    def set_label_value(self, value):
        '''
        Method that sets the slider's label according to its current value
        (the parameter 'value' is the % of the slider).

        Inputs
        ----------
        value :: int :: value in percent of the slider's parameter.
        '''
        # calculate the dimensional parameter from the value
        if self.i_log:
            # if a logarithmical evolution is used
            x = log10(self.minimum) + \
                (float(value)/(self.slider.maximum()-self.slider.minimum())
                 )*(log10(self.maximum)-log10(self.minimum))
            self.x = 10**x
        else:
            # if a linear evolution is used
            self.x = self.minimum + \
                (float(value)/(self.slider.maximum()-self.slider.minimum())
                 )*(self.maximum-self.minimum)
        # print the new value
        if self.i_multiple and \
                self.p_full_name == 'number of peak wavelengths':
            # adapt the printed number of peak periods if the domain is
            # replicated multiple (2*2 = 4) times
            self.label.setText(self.p_full_name + r': {:.2f}'.format(2*self.x))
        elif self.p_full_name == 'characteristic steepness':
            self.label.setText(self.p_full_name +
                               r': {:.2f} [%]'.format(100*self.x))
        else:
            self.label.setText(self.p_full_name + r': {:.2f}'.format(self.x))
        # center the label
        self.label.setAlignment(Qt.QtCore.Qt.AlignCenter)


class InteractiveApp(QWidget):
    '''
    Class that defines the Qt GUI containing wave field visualization, sliders
    and comboboxes.
    '''

    def __init__(self, OS, **kwargs):
        '''
        Constructor.

        Inputs
        ----------
        OS :: OceanSurface :: Wave field simulation.

        kwargs
        ----------
        fps :: float :: Number of frames per second.
        i_multiple :: bool :: If True, the domain is replicated multiple times.
        i_sound :: bool :: If True, the sound stream is activated.
        plotting_style :: string :: What to plot, 'mesh' or 'points'.
        mds :: string :: Mesh display style, 'wireframe' or 'faces'.
        pds :: string :: Points display style, one of the setGLOptions.
        point_size :: float :: Size of the displayed points
        mapped_quantity :: string :: Quantity mapped by the colormap,
                                     'elevation', 'veloctiy x', 'veloctiy y',
                                     'veloctiy z', 'absolute veloctiy',
                                     'slope x', 'slope y' or 'absolute slope'.
        colormap :: string :: Colormap, see the available colormaps provided by
                              the class CustomColorMaps.
        hue :: float :: Hue correction applied to the colormap, in [0., 1.].
        sat :: float :: Saturation correction applied to the colormap, in
                        [-1., 1.].
        val :: float :: Value (lightness) correction applied to the colormap,
                        in [-1., 1.].
        vmin :: float :: Mininmal colormap value.
        vmax :: float :: Maxinmal colormap value.
        fov :: float :: Field of view of the camera in degrees.
        elevation :: float :: Elevation angle the camera in degrees.
        azimuth :: float :: Azimuth angle of the camera in degrees.
        center :: 1darray :: Center point's coordinates in meters of the
                             camera's view. Values are normalized by the peak
                             wavelength.
        distance :: float :: Distance in meters of the camera from the center
                             point. The value is normalized by the peak
                             wavelength.
        '''
        super(InteractiveApp, self).__init__(parent=None)
        # assign the instance the input wave field simulation
        self.OS = OS
        # get the optional parameters (except camera parameters)
        self.fps = kwargs.get('fps', 25.)
        self.i_multiple = kwargs.get('i_multiple', False)
        self.i_sound = kwargs.get('i_sound', False)
        self.plotting_style = kwargs.get('plotting_style', 'mesh')
        self.mds = kwargs.get('mds', 'faces')
        self.pds = kwargs.get('pds', 'additive')
        self.point_size = kwargs.get('point_size', 3.)
        self.mapped_quantity = kwargs.get('mapped_quantity', 'elevation')
        self.colormap = kwargs.get('colormap', 'erdc_blue2cyan_BW')
        self.hue = kwargs.get('hue', 0.)
        self.sat = kwargs.get('sat', 0.)
        self.val = kwargs.get('val', 0.)
        self.vmin = kwargs.get('vmin', -0.75)
        self.vmax = kwargs.get('vmax', 0.75)
        # set the size of the GUI window
        self.resize(1250, 900)
        # create the layout
        gridLayout = QGridLayout(self)
        # size of the wave field plot (number of lines and columns in the GUI)
        nlin, ncol = 17, 17
        # create the openGL widget for the 3D visualization
        self.w = opengl.GLViewWidget()
        # set the camera position and viewing parameters
        self.w.opts['fov'] = kwargs.get('fov', 60.)
        self.w.opts['elevation'] = kwargs.get('elevation', 90.)
        self.w.opts['azimuth'] = kwargs.get('azimuth', 180.)
        self.w.opts['center'] = Vector(kwargs.get('center', [0.5, 0.5, 0.]))
        self.w.opts['distance'] = kwargs.get('distance', 0.86)
        # add the 3D plot widget to the layout
        gridLayout.addWidget(self.w, 1, 1, nlin, ncol)
        # index of line for widgets
        ii = 1
        # add sliders that control the wave field and visualization parameters
        # wave spectrum parameters
        # peak period
        self.sl_Tp = Slider(r'Tp', r'peak period [s]', self.OS.Tp,
                            0.1, 100.)
        gridLayout.addWidget(self.sl_Tp, ii, ncol+1, 1, 2)
        ii += 1
        # peak-enhancement factor
        self.sl_gamma = Slider(r'gamma', r'peak enhancement', self.OS.gamma,
                               1., 1000.)
        gridLayout.addWidget(self.sl_gamma, ii, ncol+1, 1, 2)
        ii += 1
        # characteristic steepness
        self.sl_eps = Slider(r'eps', r'characteristic steepness', self.OS.eps,
                             0.005, 0.5)
        gridLayout.addWidget(self.sl_eps, ii, ncol+1, 1, 2)
        ii += 1
        # theta_dir: main direction of propagation in degrees
        self.sl_theta_dir = Slider(r'theta_dir',
                                   r'main direction of propagation [°]',
                                   self.OS.theta_dir, 0., 360.)
        gridLayout.addWidget(self.sl_theta_dir, ii, ncol+1, 1, 2)
        ii += 1
        # directional spreading factor
        self.sl_s = Slider(r's', r'directional spreading', self.OS.s,
                           0.01, 1000.)
        gridLayout.addWidget(self.sl_s, ii, ncol+1, 1, 2)
        ii += 1
        # number of peak wavelengths in each direction
        self.sl_NL = Slider(r'NL', r'number of peak wavelengths', self.OS.NL,
                            1., 10., self.i_multiple)
        gridLayout.addWidget(self.sl_NL, ii, ncol+1, 1, 2)
        ii += 1
        # non-dimentional water depth mu = kp*depth
        self.sl_mu = Slider(r'mu', r'characteristic dispersion', self.OS.mu,
                            0.1, 10.)
        gridLayout.addWidget(self.sl_mu, ii, ncol+1, 1, 2)
        ii += 1
        # add comboboxes for some image parameters
        # ploting style
        self.cb_pls = ComboBox([r'mesh', r'points'],
                               r'ploting style ', self.plotting_style)
        gridLayout.addWidget(self.cb_pls, ii, ncol+1, 1, 1)
        # colormap
        self.cb_cm = ComboBox([r'haze_cyan', r'haze_cyan_r',
                               r'inferno', r'inferno_r',
                               r'magma', r'magma_r',
                               r'PiYG', r'PiYG_r',
                               r'erdc_blue2cyan_BW', r'erdc_blue2cyan_BW_r',
                               r'viridis', r'viridis_r',
                               r'parula', r'parula_r',
                               r'x_ray', r'x_ray_r',
                               r'RF', r'RF_r',
                               r'LF', r'LF_r',
                               r'ASJ', r'ASJ_r'],
                              r'colormap', self.colormap)
        gridLayout.addWidget(self.cb_cm, ii, ncol+2, 1, 1)
        ii += 1
        # quantity to map
        self.cb_mq = ComboBox([r'free surface elevation η/Hₛ',
                               r'orbital velocity x-component U/(4σᴜ)',
                               r'orbital velocity y-component V/(4σᴠ)',
                               r'orbital velocity z-component W/(4σᴡ)',
                               r'absolute orbital velocity (v-μᵥ)/(4σᵥ) ' +
                               r'with v²=U²+V²+W²',
                               r'slope x-direction',
                               r'slope y-direction',
                               r'absolute slope'],
                              r'mapped quantity', self.mapped_quantity,
                              kw_list=[r'elevation', r'velocity x',
                                       r'velocity y', r'velocity z',
                                       r'absolute velocity', r'slope x',
                                       r'slope y', r'absolute slope'])
        gridLayout.addWidget(self.cb_mq, ii, ncol+1, 1, 2)
        ii += 1
        # mesh display style
        self.cb_mds = ComboBox([r'wireframe', r'faces'],
                               r'mesh display style', self.mds)
        gridLayout.addWidget(self.cb_mds, ii, ncol+1, 1, 1)
        # points display style
        self.cb_pds = ComboBox([r'opaque', r'translucent', r'additive'],
                               r'points display style', self.pds)
        gridLayout.addWidget(self.cb_pds, ii, ncol+2, 1, 1)
        ii += 1
        # add sliders for other image parameters
        # point size
        self.sl_pts = Slider(r'pts', r'point size [px]',
                             self.point_size, 0.1, 20)
        gridLayout.addWidget(self.sl_pts, ii, ncol+1, 1, 2)
        ii += 1
        # mininmal colormap value
        self.sl_vmin = Slider(r'vmin', r'mininmal colormap value',
                              self.vmin, -1.5, 1.5)
        gridLayout.addWidget(self.sl_vmin, ii, ncol+1, 1, 2)
        ii += 1
        # mininmal colormap value
        self.sl_vmax = Slider(r'vmax', r'maximal colormap value',
                              self.vmax, -1.5, 1.5)
        gridLayout.addWidget(self.sl_vmax, ii, ncol+1, 1, 2)
        ii += 1
        # hue correction
        self.sl_hue = Slider(r'hue', r'hue correction', self.hue, 0., 1.)
        gridLayout.addWidget(self.sl_hue, ii, ncol+1, 1, 2)
        ii += 1
        # saturation correction
        self.sl_sat = Slider(r'sat', r'saturation correction',
                             self.sat, -1., 1.)
        gridLayout.addWidget(self.sl_sat, ii, ncol+1, 1, 2)
        ii += 1
        # value (lightness) correction
        self.sl_val = Slider(r'val', r'value (lightness) correction',
                             self.val, -1., 1.)
        gridLayout.addWidget(self.sl_val, ii, ncol+1, 1, 2)
        ii += 1
        # add a label to print the camera parameters
        self.get_text_cam_params()
        self.lb_cam = QLabel(self.cam_params)
        # center the text
        self.lb_cam.setAlignment(Qt.QtCore.Qt.AlignCenter)
        gridLayout.addWidget(self.lb_cam, ii, ncol+1, 1, 2)
        # update if sliders' values has been modified
        self.sl_Tp.slider.valueChanged.connect(self.set_sl_Tp_state_to_true)
        self.sl_gamma.slider.valueChanged.connect(
            self.set_sl_gamma_state_to_true)
        self.sl_eps.slider.valueChanged.connect(self.set_sl_eps_state_to_true)
        self.sl_theta_dir.slider.valueChanged.connect(
            self.set_sl_theta_dir_state_to_true)
        self.sl_s.slider.valueChanged.connect(self.set_sl_s_state_to_true)
        self.sl_NL.slider.valueChanged.connect(self.set_sl_NL_state_to_true)
        self.sl_mu.slider.valueChanged.connect(self.set_sl_mu_state_to_true)
        self.sl_pts.slider.valueChanged.connect(self.set_sl_pts_state_to_true)
        self.sl_vmin.slider.valueChanged.connect(
            self.set_sl_vmin_state_to_true)
        self.sl_vmax.slider.valueChanged.connect(
            self.set_sl_vmax_state_to_true)
        self.sl_hue.slider.valueChanged.connect(self.set_sl_hue_state_to_true)
        self.sl_sat.slider.valueChanged.connect(self.set_sl_sat_state_to_true)
        self.sl_val.slider.valueChanged.connect(self.set_sl_val_state_to_true)
        # update if comboboxes' values have been modified
        self.cb_pls.combobox.currentTextChanged.connect(
            self.set_cb_pls_state_to_true)
        self.cb_cm.combobox.currentTextChanged.connect(
            self.set_cb_cm_state_to_true)
        self.cb_mq.combobox.currentTextChanged.connect(
            self.set_cb_mq_state_to_true)
        self.cb_mds.combobox.currentTextChanged.connect(
            self.set_cb_mds_state_to_true)
        self.cb_pds.combobox.currentTextChanged.connect(
            self.set_cb_pds_state_to_true)
        # initiate the wave field 3D plot
        self.init_plot()

    def get_text_cam_params(self):
        '''
        Method that constructs the text displaying the camera parameters.
        '''
        self.cam_params = '𝐜𝐚𝐦𝐞𝐫𝐚 𝐩𝐚𝐫𝐚𝐦𝐞𝐭𝐞𝐫𝐬\n' + \
            'fov: {:.2f}° ― '.format(self.w.opts['fov']) + \
            'elevation: {:.2f}° ― '.format(self.w.opts['elevation']) + \
            'azimuth: {:.2f}°'.format(self.w.opts['azimuth']) + '\n' + \
            'center: ({:.2f}, {:.2f}, {:.2f}) ― '.format(
                self.w.opts['center'][0],
                self.w.opts['center'][1],
                self.w.opts['center'][2]) + \
            'distance: {:.2f}'.format(self.w.opts['distance'])

    def set_sl_Tp_state_to_true(self):
        '''
        Method that sets widget 1's state (boolean stating that value have been
        modified) to True. Following methods are similar but applied to other
        widgets.
        '''
        if not self.sl_Tp.state:
            self.sl_Tp.state = True

    def set_sl_gamma_state_to_true(self):
        if not self.sl_gamma.state:
            self.sl_gamma.state = True

    def set_sl_eps_state_to_true(self):
        if not self.sl_eps.state:
            self.sl_eps.state = True

    def set_sl_theta_dir_state_to_true(self):
        if not self.sl_theta_dir.state:
            self.sl_theta_dir.state = True

    def set_sl_s_state_to_true(self):
        if not self.sl_s.state:
            self.sl_s.state = True

    def set_sl_NL_state_to_true(self):
        if not self.sl_NL.state:
            self.sl_NL.state = True

    def set_sl_mu_state_to_true(self):
        if not self.sl_mu.state:
            self.sl_mu.state = True

    def set_sl_pts_state_to_true(self):
        if not self.sl_pts.state:
            self.sl_pts.state = True

    def set_sl_vmin_state_to_true(self):
        if not self.sl_vmin.state:
            self.sl_vmin.state = True

    def set_sl_vmax_state_to_true(self):
        if not self.sl_vmax.state:
            self.sl_vmax.state = True

    def set_sl_hue_state_to_true(self):
        if not self.sl_hue.state:
            self.sl_hue.state = True

    def set_sl_sat_state_to_true(self):
        if not self.sl_sat.state:
            self.sl_sat.state = True

    def set_sl_val_state_to_true(self):
        if not self.sl_val.state:
            self.sl_val.state = True

    def set_cb_pls_state_to_true(self):
        if not self.cb_pls.state:
            self.cb_pls.state = True

    def set_cb_cm_state_to_true(self):
        if not self.cb_cm.state:
            self.cb_cm.state = True

    def set_cb_mq_state_to_true(self):
        if not self.cb_mq.state:
            self.cb_mq.state = True

    def set_cb_mds_state_to_true(self):
        if not self.cb_mds.state:
            self.cb_mds.state = True

    def set_cb_pds_state_to_true(self):
        if not self.cb_pds.state:
            self.cb_pds.state = True

    def get_time(self, seconds):
        '''
        Method that splits a number of seconds into minutes, seconds and
        centiseconds, and gives the result as a string.

        Inputs
        ----------
        seconds :: float :: number of seconds.

        Outputs
        ----------
        time :: string :: string indicating the number of minutes, seconds and
                          centiseconds in the input number of seconds.
        '''
        result = []
        # calculate the number of minutes, seconds and centiseconds
        for count in [60, 1, 0.01]:
            value = seconds//count
            # append the results
            if value:
                seconds -= value*count
                result.append('{:02.0f}'.format(value))
            else:
                value = 0.
                result.append('{:02.0f}'.format(value))
        # join the results, separated by ':'
        time = 'time is ' + ':'.join(result[:])
        return time

    def update_time(self):
        '''
        Method that updates the time-dependent parameters of the simulation.
        '''
        # increment index of current time
        self.time_index += 1
        # calculate the time in seconds
        time = self.time_index/self.fps
        # print the current time
        sys.stdout.write('\r' + self.get_time(time))
        sys.stdout.flush()
        # update the parameters of the wave field
        self.update_wave_field_params()
        # get the current surface elevation
        self.OS.get_elevation(self.dt)
        # update the 3D plot
        self.plot_data()
        # update print of camera parameters
        self.get_text_cam_params()
        self.lb_cam.setText(self.cam_params)
        if self.i_sound:
            # update the sound data, if sound stream activated
            # get the surface elevation at a boundary
            self.OS.get_elevation_as_spectrum(self.fps, self.time_index)
            # get the sound signal
            self.OS.get_sound_signal(self.fps)
            # put the sound signal in the queue
            self.sound.put((self.OS.sound*32768).astype(int16))

    def update_wave_field_params(self):
        '''
        Method that updates the modified wave field parameters. It checks if
        the state of the parameters have been modified, updates the impacted
        quantities, and sets the corresponding states to False.
        '''
        # update the wave field inputs if they have been modified
        if array([self.sl_Tp.state, self.sl_gamma.state, self.sl_eps.state,
                  self.sl_theta_dir.state, self.sl_s.state, self.sl_NL.state,
                  self.sl_mu.state]).any():
            self.OS.update_inputs(Tp=self.sl_Tp.x,
                                  gamma=self.sl_gamma.x,
                                  eps=self.sl_eps.x,
                                  theta_dir=self.sl_theta_dir.x,
                                  s=self.sl_s.x,
                                  NL=self.sl_NL.x,
                                  mu=self.sl_mu.x)
        # check if the wave spectrum of the wave field has been modified
        if self.sl_Tp.state or self.sl_gamma.state or self.sl_NL.state \
                or self.sl_mu.state:
            # parameters related to the wavenumber spectrum
            self.update_wavenumber_spectrum()
            self.sl_Tp.state, self.sl_gamma.state, self.sl_NL.state, \
                self.sl_mu.state = False, False, False, False
        elif self.sl_theta_dir.state or self.sl_s.state:
            # parameters related to the directional spreading
            self.update_dir_spectrum()
            self.sl_theta_dir.state, self.sl_s.state = False, False
        elif self.sl_eps.state:
            # wave steepness (equivalent to spectral amplitude, as the period
            # actually stays constant)
            self.update_steepness()
            self.sl_eps.state = False
        # check if the mapped quantity of the visualization has been modified
        if self.cb_mq.state:
            self.update_mapped_quantity()
            self.cb_mq.state = False
        # check if the mesh display style of the visualization has been
        # modified
        if self.cb_mds.state:
            self.update_mesh_display_style()
            self.cb_mds.state = False
        # check if the points display style of the visualization has been
        # modified
        if self.cb_pds.state:
            self.update_points_display_style()
            self.cb_pds.state = False
        # check if the point size of the visualization has been modified
        if self.sl_pts.state:
            self.update_point_size()
            self.sl_pts.state = False
        # check if the colormap limits of the visualization have been modified
        if self.sl_vmin.state or self.sl_vmax.state:
            self.update_cmap_limits()
            self.sl_vmin.state, self.sl_vmax.state = False, False
        # check if the colormap of the visualization has been modified
        if self.sl_hue.state or self.sl_sat.state or self.sl_val.state or \
                self.cb_cm.state:
            self.update_colormap()
            self.sl_hue.state, self.sl_sat.state, self.sl_val.state, \
                self.cb_cm.state = False, False, False, False
        # check if the plotting style of the visualization has been modified
        if self.cb_pls.state:
            self.update_plotting_style()
            self.cb_pls.state = False

    def init_plot(self):
        '''
        Method that initiates the 3D plot.
        '''
        # initiate the wave field inputs
        self.OS.update_inputs(gamma=self.sl_gamma.x,
                              eps=self.sl_eps.x,
                              theta_dir=self.sl_theta_dir.x,
                              s=self.sl_s.x,
                              NL=self.sl_NL.x,
                              mu=self.sl_mu.x)
        # get some wave field simulation parameters
        self.OS.get_sim_params()
        # get the initial wavenumber spectrum
        self.OS.get_wavenumber_spectrum()
        # get the initial (directional) wave spectrum
        self.OS.get_directional_spectrum()
        # get the initial spectral amplitudes
        self.OS.get_modal_amplitudes()
        # initiate the time index
        self.time_index = 0
        # calculate the time step according to the fps
        self.dt = 1./self.fps
        # get the initial surface elevation
        self.OS.get_elevation(self.dt)
        # create an instance of custom colormaps
        self.CCM = CustomColorMaps()
        # load the initial colormap
        cm_params = self.CCM.load(self.cb_cm.combobox.currentText())
        # apply the intial HSV corrections
        cm_params = self.CCM.apply_correction(cm_params,
                                              self.sl_hue.x,
                                              self.sl_sat.x,
                                              self.sl_val.x)
        # create the colormap
        self.cm = ColorMap(cm_params[:, 0], cm_params[:, 1:])
        # set the 'modified plotting style state' to False
        self.mod_style_state = False
        # plot the initial data
        self.plot_data()
        if self.i_sound:
            # initiate the sound data, if sound stream activated
            # create the queue for the sound signal
            self.sound = Queue(maxsize=1)
            # get the surface elevation at a boundary
            self.OS.get_elevation_as_spectrum(self.fps, self.time_index)
            # get the sound signal
            self.OS.get_sound_signal(self.fps, coef=1)
            # put the sound signal in the queue
            self.sound.put((self.OS.sound*32768).astype(int16))

    def update_wavenumber_spectrum(self):
        '''
        Method that updates the relevant parameters of the wave model to take
        into account a modification of the wavenumber spectrum.
        '''
        self.OS.get_sim_params()
        self.OS.get_wavenumber_spectrum()
        self.OS.get_directional_spectrum()
        self.OS.get_modal_amplitudes()

    def update_steepness(self):
        '''
        Method that updates the spectral amplitudes to take into account a
        modification of the wave steepness.
        '''
        self.OS.get_modal_amplitudes()

    def update_dir_spectrum(self):
        '''
        Method that updates the relevant parameters of the wave model to take
        into account a modification of the directional spreading of the wave
        spectrum.
        '''
        self.OS.get_directional_spectrum()
        self.OS.get_modal_amplitudes()

    def update_colormap(self):
        '''
        Method that updates the colormap of the 3D plot to take into account
        modifications of the colormap type and its HSV corrections.
        '''
        cm_params = self.CCM.load(self.cb_cm.combobox.currentText())
        cm_params = self.CCM.apply_correction(cm_params,
                                              self.sl_hue.x,
                                              self.sl_sat.x,
                                              self.sl_val.x)
        self.cm = ColorMap(cm_params[:, 0], cm_params[:, 1:])

    def update_point_size(self):
        '''
        Methods that updates the size of the plotted points.
        '''
        self.point_size = self.sl_pts.x

    def update_cmap_limits(self):
        '''
        Methods that updates the limits of the colormap.
        '''
        # extreme colormap values for the mapped quantity
        self.vmin, self.vmax = self.sl_vmin.x, self.sl_vmax.x

    def update_points_display_style(self):
        '''
        Methods that updates the points display style of the 3D plot.
        '''
        self.pds = self.cb_pds.combobox.currentText()

    def update_mesh_display_style(self):
        '''
        Methods that updates the mesh display style of the 3D plot.
        '''
        self.mds = self.cb_mds.combobox.currentText()

    def update_mapped_quantity(self):
        '''
        Methods that updates the mapped quantity in the 3D plot.
        '''
        index = self.cb_mq.combobox.findText(self.cb_mq.combobox.currentText(),
                                             Qt.QtCore.Qt.MatchFixedString)
        self.mapped_quantity = self.cb_mq.kw_list[index]

    def update_plotting_style(self):
        '''
        Methods that updates the plotting style in the 3D plot.
        '''
        self.plotting_style = self.cb_pls.combobox.currentText()
        self.mod_style_state = True

    def plot_data(self):
        '''
        Methods that updates the plotted data.
        '''
        # build the matrix of positions of the free surface particles
        if self.i_multiple:
            # if the domain is replicated multiple (2*2 = 4) times
            d = self.OS.L+0.5*(self.OS.beta[-1, 1]-self.OS.beta[-1, 0])
            self.OS.x = vstack((hstack((self.OS.x, self.OS.x)),
                                hstack((self.OS.x+d, self.OS.x+d))))
            self.OS.y = vstack((hstack((self.OS.y, self.OS.y+d)),
                                hstack((self.OS.y, self.OS.y+d))))
            self.OS.z = vstack((hstack((self.OS.z, self.OS.z)),
                                hstack((self.OS.z, self.OS.z))))
        # update the colors
        if not (self.plotting_style == 'mesh' and self.mds == 'wireframe'):
            # if the plot is not the wireframe, update the mapped quantity
            if self.mapped_quantity == 'elevation':
                quantity = self.OS.z/self.OS.Hs
            elif self.mapped_quantity == 'velocity x':
                self.OS.get_velocity('x')
                quantity = self.OS.U/(4*self.OS.U.std())
            elif self.mapped_quantity == 'velocity y':
                self.OS.get_velocity('y')
                quantity = self.OS.V/(4*self.OS.V.std())
            elif self.mapped_quantity == 'velocity z':
                self.OS.get_velocity('z')
                quantity = self.OS.W/(4*self.OS.W.std())
            elif self.mapped_quantity == 'absolute velocity':
                self.OS.get_velocity('all')
                quantity = (self.OS.vel-self.OS.vel.mean()) / \
                    (4*self.OS.vel.std())
            elif self.mapped_quantity == 'slope x':
                self.OS.get_slope('x')
                quantity = self.OS.sx
            elif self.mapped_quantity == 'slope y':
                self.OS.get_slope('y')
                quantity = self.OS.sy
            elif self.mapped_quantity == 'absolute slope':
                self.OS.get_slope('all')
                quantity = self.OS.slope
            else:
                # if the chosen quantity to map is not valid
                sys.exit('Please choose a valid quantity to map.')
            # adapt the value of the mapped quantity to the colormap limits
            if self.vmin != self.vmax:
                quantity = (quantity-self.vmin)/(self.vmax-self.vmin)
            else:
                # set the quantity to zero if the minimum and maximum limits of
                # the colormap are equal
                quantity *= 0.
            # map the quantity with the colormap
            self.color = self.cm.map(quantity)/255.
        # update the visualization
        if self.plotting_style == 'mesh':
            # if the meah is plotted
            # get the mesh
            self.get_mesh()
            # update the mesh display style
            if self.mds == 'faces':
                drawEdges = False
                drawFaces = True
            elif self.mds == 'wireframe':
                drawEdges = True
                drawFaces = False
            else:
                # exit if mesh display style is not valid
                sys.exit('Please choose a valid mesh display style.')
            # plot the updated data and colormap
            if self.time_index == 0 or self.mod_style_state:
                # if it is the first time step or the plotting style has been
                # modified
                if self.mod_style_state:
                    # if the plotting style has been modified
                    # remove the previous plot
                    self.w.removeItem(self.sp)
                    # set the 'modified plotting style state' to False
                    self.mod_style_state = False
                # create the openGL widget containing the 3D plot
                self.sp = opengl.GLMeshItem(meshdata=self.mesh,
                                            edgeColor=(1., 1., 1., 1.),
                                            drawEdges=drawEdges,
                                            drawFaces=drawFaces,
                                            smooth=False,
                                            computeNormals=False)
                # add the plot to the openGL widget
                self.w.addItem(self.sp)
            else:
                # update the already existing 3D plot
                self.sp.setMeshData(meshdata=self.mesh,
                                    drawEdges=drawEdges,
                                    drawFaces=drawFaces)
        elif self.plotting_style == 'points':
            # if the points is plotted
            # construct the points' position matrix
            pos = stack((self.OS.x.ravel(),
                         self.OS.y.ravel(),
                         self.OS.z.ravel())).T
            # plot the updated data and colormap
            if self.time_index == 0 or self.mod_style_state:
                # if it is the first time step or the plotting style has been
                # modified
                if self.mod_style_state:
                    # if the plotting style has been modified
                    # remove the previous plot
                    self.w.removeItem(self.sp)
                    # set the 'modified plotting style state' to False
                    self.mod_style_state = False
                # create the openGL widget containing the 3D plot
                self.sp = opengl.GLScatterPlotItem(pos=pos/self.OS.L,
                                                   color=self.color,
                                                   size=self.point_size,
                                                   glOptions=self.pds)
                # add the plot to the openGL widget
                self.w.addItem(self.sp)
            else:
                # update the already existing 3D plot
                self.sp.setData(pos=pos/self.OS.L, color=self.color,
                                size=self.point_size)
                # set the points display style
                self.sp.setGLOptions(self.pds)
        else:
            # exit if plotting style is not valid
            sys.exit('Please choose a valid plotting style.')

    def get_mesh(self):
        '''
        Method that constructs the matrices containing the (x,y,z) coordinates
        and corresponding colors of the mesh vertices.
        '''
        # construct the matrix of (x,y,z) coordinates of the mesh vertices
        # the size is (Nf, 3, 3), with Nf the number of triangular faces
        set1 = array([[self.OS.x[:-1, :-1].ravel(),
                       self.OS.y[:-1, :-1].ravel(),
                       self.OS.z[:-1, :-1].ravel()],
                      [self.OS.x[1:, :-1].ravel(),
                       self.OS.y[1:, :-1].ravel(),
                       self.OS.z[1:, :-1].ravel()],
                      [self.OS.x[:-1, 1:].ravel(),
                       self.OS.y[:-1, 1:].ravel(),
                       self.OS.z[:-1, 1:].ravel()]])
        set2 = array([[self.OS.x[1:, 1:].ravel(),
                       self.OS.y[1:, 1:].ravel(),
                       self.OS.z[1:, 1:].ravel()],
                      [self.OS.x[:-1, 1:].ravel(),
                       self.OS.y[:-1, 1:].ravel(),
                       self.OS.z[:-1, 1:].ravel()],
                      [self.OS.x[1:, :-1].ravel(),
                       self.OS.y[1:, :-1].ravel(),
                       self.OS.z[1:, :-1].ravel()]])
        vertexCoord = moveaxis(concatenate(
            (set1/self.OS.L, set2/self.OS.L), axis=-1), -1, 0)
        # construct the matrix of RGBalpha colors of the mesh vertices
        # the size is (Nf, 3, 4), with Nf the number of triangular faces
        if self.mds != 'wireframe':
            # if the plot is not the wireframe, update the vertex colors
            col1 = array([[self.color[:-1, :-1, 0].ravel(),
                           self.color[:-1, :-1, 1].ravel(),
                           self.color[:-1, :-1, 2].ravel(),
                           self.color[:-1, :-1, 3].ravel()],
                          [self.color[1:, :-1, 0].ravel(),
                           self.color[1:, :-1, 1].ravel(),
                           self.color[1:, :-1, 2].ravel(),
                           self.color[1:, :-1, 3].ravel()],
                          [self.color[:-1, 1:, 0].ravel(),
                           self.color[:-1, 1:, 1].ravel(),
                           self.color[:-1, 1:, 2].ravel(),
                           self.color[:-1, 1:, 3].ravel()]])
            col2 = array([[self.color[1:, 1:, 0].ravel(),
                           self.color[1:, 1:, 1].ravel(),
                           self.color[1:, 1:, 2].ravel(),
                           self.color[1:, 1:, 3].ravel()],
                          [self.color[:-1, 1:, 0].ravel(),
                           self.color[:-1, 1:, 1].ravel(),
                           self.color[:-1, 1:, 2].ravel(),
                           self.color[:-1, 1:, 3].ravel()],
                          [self.color[1:, :-1, 0].ravel(),
                           self.color[1:, :-1, 1].ravel(),
                           self.color[1:, :-1, 2].ravel(),
                           self.color[1:, :-1, 3].ravel()]])
            vertexColors = moveaxis(concatenate((col1, col2), axis=-1), -1, 0)
        else:
            vertexColors = zeros((vertexCoord.shape[0], 3, 4))
        # get the mesh according to the pyqtgraph.opengl formalism
        self.mesh = opengl.MeshData(vertexes=vertexCoord,
                                    vertexColors=vertexColors)

    def stream_callback(self, in_data, frame_count, time_info, status):
        '''
        Methods that is called by the opened pyaudio stream.

        Inputs
        ----------
        in_data :: 1darray :: recorded data if input=True; else None.
        frame_count :: int :: number of frames.
        time_info :: dict :: dictionary with the following keys:
                             ``input_buffer_adc_time``, ``current_time`` and
                             ``output_buffer_dac_time``; see the PortAudio
                             documentation for their meanings.
        status :: PaCallbackFlags :: PortAutio Callback Flag; see the PortAudio
                                     documentation for more details.

        Outputs (formatted as a tuple)
        ----------
        data :: 1darray :: data to stream in an array whose length should be
                           frame_count * channels * bytes-per-channel.
        flag :: PaCallbackReturnCodes :: flag that states if the stream should
                                         continue, is complete, or should
                                         abort.
        '''
        # get the sound signal from the queue
        data = self.sound.get().tobytes()
        # set the flag to continue to stream
        flag = pyaudio.paContinue
        return (data, flag)
