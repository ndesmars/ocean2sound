# -*- coding: utf-8 -*-
'''
For more details than those found in the documentation, code comments and
references, please let me know at:
    nicolas _dot_ desmars _at_ protonmail _dot_ com

License
----------
Copyright 2024 Nicolas Desmars

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

from numpy import pi, linspace, exp, append, sqrt, hypot, meshgrid, arctan2, \
    nonzero, deg2rad, copy, newaxis, outer, real, vstack, hstack, flipud, \
    fliplr, where, zeros_like, interp, linalg, array, dot, fft, tanh, cos, \
    random, sinh, cosh
from scipy.optimize import fsolve


class OceanSurface():
    '''
    Class that provides methods to generate a wave field realization from a
    JONSWAP spectrum. A method allows the use of an external spectrum. This
    class also contains methods for the generation of a sound signal from the
    simulated ocean data.

    References
    ----------
    - The wave spectrum follows the so-called JONSWAP formulation:

    Hasselmann, K., Barnet, T., Bouws, E., Carlson, H., Cartwright, D., Enke,
    K., Ewing, J., Gienapp, H., Hasselmann, D., Krusemann, P., Meerburg, A.,
    Müller, P., Olbers, D., Richter, K., Seil, W. & Walden, H. (1973)
    Measurements of wind-wave growth and swell decay during the JOint North Sea
    WAve Project (JONSWAP). Deutsche Hydrographische Zeitschrift, Reihe A 8
    (12), 1–95.

    - The ocean surface is modeled according to the Improved Choppy Wave Model:

    Guérin, C.-A., Desmars, N., Grilli, S. T., Ducrozet, G., Perignon, Y. &
    Ferrant, P. (2019) An improved Lagrangian model for the time evolution of
    nonlinear surface waves. Journal of Fluid Mechanics 876, 527–552.
    '''

    def __init__(self, **kwargs):
        '''
        Constructor.

        kwargs
        ----------
        Tp :: float :: Peak wave period in seconds.
        gamma :: float :: Peak-enhancement factor.
        eps :: float :: Characteristic wave steepness.
        theta_dir :: float :: Main direction of propagation in degrees.
        s :: float :: Directional spreading factor.
        NL :: float :: Number of peak wavelengths in each direction.
        mu :: float :: Non-dimensional water depth mu = kp*depth.
        phase_seed :: int :: Seed for the initial phase distribution.
        Nk :: int :: Number of wave components along each direction.
        Nt :: int :: Number of time steps for the sound generation.
        '''
        self.Tp = kwargs.get('Tp', 10.)
        self.gamma = kwargs.get('gamma', 3.3)
        self.eps = kwargs.get('eps', 0.04)
        self.theta_dir = kwargs.get('theta_dir', 0.)
        self.s = kwargs.get('s', 25.)
        self.NL = kwargs.get('NL', 8.)
        self.mu = kwargs.get('mu', 10.)
        self.phase_seed = kwargs.get('phase_seed', 0)
        self.Nk = kwargs.get('Nk', 256)
        self.Nt = kwargs.get('Nt', 1500)

    def update_inputs(self, **kwargs):
        '''
        Method that updates some inputs of the class.

        kwargs
        ----------
        Tp :: float :: Peak wave period in seconds.
        gamma :: float :: Peak-enhancement factor.
        eps :: float :: Characteristic wave steepness.
        theta_dir :: float :: Main direction of propagation in degrees.
        s :: float :: Directional spreading factor.
        NL :: float :: Number of peak wavelengths in each direction.
        mu :: float :: Non-dimensional water depth mu = kp*depth.
        '''
        self.Tp = kwargs.get('Tp', self.Tp)
        self.gamma = kwargs.get('gamma', self.gamma)
        self.eps = kwargs.get('eps', self.eps)
        self.theta_dir = kwargs.get('theta_dir', self.theta_dir)
        self.s = kwargs.get('s', self.s)
        self.NL = kwargs.get('NL', self.NL)
        self.mu = kwargs.get('mu', self.mu)

    def get_sim_params(self):
        '''
        Method that calculates some numerical parameters.
        '''
        # peak angular frequency
        self.wp = 2*pi/self.Tp
        # peak wavenumber
        self.get_kp()
        # peak wavelength
        self.Lp = 2*pi/self.kp
        # space extent of computational domain in x- and y-directions
        self.L = self.NL*self.Lp
        # wavenumber step
        self.dk = 2*pi/self.L
        # wavenumber vector
        k_vec = fft.fftshift(2*pi*fft.fftfreq(self.Nk, self.L/self.Nk))
        # wavenumber grids in x- and y-directions
        self.kx, self.ky = meshgrid(k_vec, k_vec, indexing='ij')
        # grid of absolute value of wavenumbers
        self.k = hypot(self.kx, self.ky)
        # grid of wave directions of propagation in gradients
        self.theta = arctan2(self.ky, self.kx)
        # grid of normalized wavenumbers in the x-direction (w/o singularity)
        kx_k = self.kx[self.k != 0.]/self.k[self.k != 0.]
        # grid of normalized wavenumbers in the y-direction (w/o singularity)
        ky_k = self.ky[self.k != 0.]/self.k[self.k != 0.]
        # water depth
        depth = self.mu/self.kp
        # depth-dependent term tanh(k*depth)
        tanhkh = tanh(self.k*depth)
        # depth-dependent term coth(k*depth) (w/o singularity)
        cothkh = 1/tanhkh[self.k != 0.]
        # depth-dependent term for Stokes drift (w/o singularity)
        if self.mu <= 2.:  # to avoid overflow
            Us_depth_coef = cosh(2*self.k[self.k != 0.]*depth) / \
                            (2*sinh(self.k[self.k != 0.]*depth)**2)
        # grid of angular frequencies (w/o singularity)
        w = sqrt(self.k[self.k != 0.]*9.81*tanhkh[self.k != 0.])
        # grid of group velocities (w/o singularity)
        cg = 0.5*w/self.k[self.k != 0.]
        if self.mu <= 2.:  # to avoid overflow
            cg *= (1+2*self.k[self.k != 0.]*depth /
                   sinh(2*self.k[self.k != 0.]*depth))
        # index of wavenumber equal to zero
        i0 = nonzero(self.k.ravel() == 0.)[0][0]
        # include zero in variables
        self.w = hstack((w[:i0], 0., w[i0:])).reshape(self.k.shape)
        self.cg = hstack((cg[:i0], 0., cg[i0:])).reshape(self.w.shape)
        self.kx_k = hstack((kx_k[:i0],  0., kx_k[i0:])).reshape(self.k.shape)
        self.ky_k = hstack((ky_k[:i0], 0., ky_k[i0:])).reshape(self.k.shape)
        self.cothkh = hstack((cothkh[:i0], 0., cothkh[i0:])
                             ).reshape(self.k.shape)
        if self.mu <= 2.:  # to avoid overflow
            self.Us_depth_coef = hstack((Us_depth_coef[:i0],
                                        0.,
                                        Us_depth_coef[i0:])
                                        ).reshape(self.k.shape)

    def get_wavenumber_spectrum(self):
        '''
        Method that calculates the wavenumber spectrum (i.e. elevation
        variance spectrum as a function of the wavenumber).
        '''
        # make use of spectrum symmetry: only retain one quarter of it
        wq = self.w[:self.Nk//2+1, :self.Nk//2+1]
        # indices of angular frequencies lower and higher than the peak one
        ind1, ind2 = where(wq <= self.wp),  where(wq > self.wp)
        # corresponding angular frequencies
        wq1, wq2 = wq[ind1], wq[ind2]
        # index of angular frequency equal to zero
        i0 = nonzero(wq1.ravel() == 0.)[0][0]
        # angular frequency spectrum lower than the peak (w/o zero constant)
        Ew1 = self.jonswap_spectrum_w_sigma(wq1[wq1 != 0.], 0.07)
        # angular frequency spectrum higher than the peak
        Ew2 = self.jonswap_spectrum_w_sigma(wq2, 0.09)
        # include zero constant to Ew1
        Ew1 = hstack((Ew1[:i0], 0., Ew1[i0:])).reshape(wq1.shape)
        # calculate the angular frequency spectrum
        Ew = zeros_like(wq)
        Ew[ind1], Ew[ind2] = Ew1, Ew2
        # merge the different parts (quarters)
        Ew = hstack((Ew, fliplr(Ew[:, (self.Nk+1) % 2:-1])))
        Ew = vstack((Ew, flipud(Ew)[(self.Nk+1) % 2:-1, :]))
        # calculate the wavenumber spectrum
        self.Ek = Ew*self.cg

    def get_directional_spectrum(self):
        '''
        Method that calculates the unscaled directional spectrum (i.e.
        elevation variance spectrum in the wavenumber space).
        '''
        # copy the wavenumber spectrum
        self.Sk = copy(self.Ek)
        # multiply by the directional spreading function
        self.Sk *= self.directional_spreading_cos_no_scaling()
        # devide by the absolute wavenumber
        self.Sk[self.k != 0.] /= self.k[self.k != 0.]

    def get_modal_amplitudes(self):
        '''
        Method that calculates the complex modal amplitudes of the directional
        spectrum and nonlinear quantities.
        '''
        # scale the wave spectrum according to the steepness
        # significant wave height
        self.Hs = self.eps*self.Lp
        self.Sk *= (self.Hs**2/16)/(self.Sk.sum()*self.dk**2)
        # get the spectral elevation amplitudes
        self.Ak = sqrt(2*self.Sk*self.dk**2)
        # calculate the Stokes' drift vector
        if self.mu <= 2.:  # to avoid overflow
            Us = self.Ak**2*self.w*self.Us_depth_coef
        else:
            Us = self.Ak**2*self.w
        Usx, Usy = (Us*self.kx).sum(), (Us*self.ky).sum()
        # calculate the scalar product of Stokes' drift and wavenumber vectors
        Usdotk = self.kx*Usx+self.ky*Usy
        # calculate the nonlinear angular velocity
        self.w_NL = self.w+0.5*Usdotk
        # calculate the nonlinear vertical shift
        self.z2 = 0.5*(self.Ak**2*self.k*self.cothkh).sum()

    def get_elevation(self, dt):
        '''
        Method that calculates the surface elevation from the amplitude
        spectrum according to ICWM using IFFTs. In order to take care of the
        nonlinear particle shift, the quantities are plotted on a grid
        (alpha-Usx*t, beta-Usy*t), leading the nonlinear angular frequency to
        be w_NL = w+0.5*(Usx*kx + Usy*ky).
        To avoid non-physical behavior (i.e. particles going back and forth
        according to the current steepness), the second-order velocity
        correction is implemented as a modified phase, that is updated based on
        its value at the previous time step.

        Inputs
        ----------
        dt :: float :: Time increment.
        '''
        if not hasattr(self, 'phi_k'):
            # initiate the set of phases
            # seed for random initial phases
            random.seed(self.phase_seed)
            # initial phase vector
            self.phi_k = random.rand(self.k.shape[0], self.k.shape[1])*2*pi
        else:
            # update the set of phases
            self.phi_k += self.w_NL*dt
        # calculate the complex amplitudes
        self.Ak_c = self.Ak*exp(-1j*self.phi_k)
        # shift the complex amplitudes
        Ak_shifted_kxk = 1j*self.kx_k*self.Ak_c*self.cothkh
        Ak_shifted_kyk = 1j*self.ky_k*self.Ak_c*self.cothkh
        # calculate the free surface particles' horizontal displacement
        Dx = self.elev_from_full_spectrum_3D(Ak_shifted_kxk,
                                             self.dk, self.dk)[1]
        Dy = self.elev_from_full_spectrum_3D(Ak_shifted_kyk,
                                             self.dk, self.dk)[1]
        # calculate the free surface particles' vertical displacement
        [alpha, self.beta], self.z = self.elev_from_full_spectrum_3D(
            self.Ak_c, self.dk, self.dk)
        # add horizontal displacements to reference locations
        self.x, self.y = alpha+Dx, self.beta+Dy
        # add second-order vertical displacement
        self.z += self.z2

    def get_velocity(self, coord):
        '''
        Method that calculates the surface orbital velocities and the absolute
        velocity from the amplitude spectrum according to ICWM using IFFTs.

        Inputs
        ----------
        coord :: string :: Component to calculate.
        '''
        if coord == 'all' or coord == 'x':
            # calculate the x-component of the time-derived complex amplitudes
            Ak_kxk_w = self.kx_k*self.w_NL*self.Ak_c*self.cothkh
            # calculate the x-component of the orbital velocity
            self.U = self.elev_from_full_spectrum_3D(Ak_kxk_w,
                                                     self.dk, self.dk)[1]
        if coord == 'all' or coord == 'y':
            # calculate the y-component of the time-derived complex amplitudes
            Ak_kyk_w = self.ky_k*self.w_NL*self.Ak_c*self.cothkh
            # calculate the y-component of the orbital velocity
            self.V = self.elev_from_full_spectrum_3D(Ak_kyk_w,
                                                     self.dk, self.dk)[1]
        if coord == 'all' or coord == 'z':
            # calculate the z-component of the time-derived complex amplitudes
            Ak_shifted_w = -1j*self.w_NL*self.Ak_c
            # calculate the z-component of the orbital velocity
            self.W = self.elev_from_full_spectrum_3D(Ak_shifted_w,
                                                     self.dk, self.dk)[1]
        if coord == 'all':
            # calculate the absolute particles' velocity
            self.vel = sqrt(self.U**2+self.V**2+self.W**2)

    def get_slope(self, coord):
        '''
        Method that calculates the surface slopes and the absolute slope from
        the amplitude spectrum according to ICWM using IFFTs.

        Inputs
        ----------
        coord :: string :: Component to calculate.
        '''
        if coord == 'all' or coord == 'x':
            # calculate the x-gradient of the complex amplitudes
            Ak_shifted_kx = 1j*self.kx*self.Ak_c
            # calculate the x-component of the slope
            self.sx = self.elev_from_full_spectrum_3D(Ak_shifted_kx,
                                                      self.dk, self.dk)[1]
        if coord == 'all' or coord == 'y':
            # calculate the y-gradient of the complex amplitudes
            Ak_shifted_ky = 1j*self.ky*self.Ak_c
            # calculate the y-component of the slope
            self.sy = self.elev_from_full_spectrum_3D(Ak_shifted_ky,
                                                      self.dk, self.dk)[1]
        if coord == 'all':
            # calculate the absolute particles' velocity
            self.slope = sqrt(self.sx**2+self.sy**2)

    def get_elevation_as_spectrum(self, fps, time_index):
        '''
        Method that interpretes the surface elevation on the x = L boundary as
        a spectrum for the generation of the audio. As the surface elevation
        changes in time, a spectrogram is obtained. It is later used to
        generate a sound signal.

        Inputs
        ----------
        fps :: int :: Number of frames per second.
        time_index :: int :: Index of the current time.
        '''
        if time_index == 0:
            # initiate the frequency vector of the spectrum
            # maximal frequency (Nyquist frequency) divided by an arbitrary
            # coefficient
            fmax = 0.5*(self.Nt-1)*fps/1
            # minimal frequency of the audible spectrum
            fmin = 20.
            # number of frequency = number of points on the domain boundary
            self.Nf = self.Nk+1
            # get a random number for each frequency
            self.rand_num = random.rand(self.Nf)
            # sort the random numbers
            self.rand_num.sort()
            # calculate frequencies randomly distributed in [fmin, fmax]
            self.f = fmin+self.rand_num*(fmax-fmin)
            # damp high frequencies with an exponential filter
            self.F = exp(-self.f*1e-3)
        # get corresponding locations on the domain boundary
        self.loc = self.beta[-1, -1]+self.rand_num*(self.beta[-1, 1] -
                                                    self.beta[-1, -1])
        # interpolate (linearly) the surface elevation at the locations
        elev_map = interp(self.loc, self.y[-1, :], self.z[-1, :])
        # normalize the surface first so the exponential function (see below)
        # is not scaled according to the absolute surface elevation
        elev_map /= 4*elev_map.std()
        # make the amplitudes positive and more distinct (increase the dynamic
        # range)
        self.elev_map = 30**elev_map
        # apply the exponential filter
        self.elev_map *= self.F
        # make the lowest amplitude be zero
        self.elev_map -= self.elev_map.min()

    def get_sound_signal(self, fps, **kwargs):
        '''
        Method that calculates an audio signal from a spectrum mapped on the
        free surface elevation obtained at one boundary of the domain.

        Inputs
        ----------
        fps :: int :: Number of frames per second.

        kwargs
        ----------
        phi_t :: 2darray :: Array of phases and their 'propagation' along the
                            time vector t.
        coef :: int :: Coefficient used to define a subgrid of frequencies for
                       the sound signal calculation. This can help decreasing
                       the computational time.
        i_seed :: int :: Seed for the initial phase distribution.
        '''
        # calculate the time vector
        self.t = linspace(0., 1/fps, self.Nt)
        if not hasattr(self, 'phi_t'):
            # initiate the set of phases
            # coefficient to define a subgrid of frequency
            self.c = kwargs.get('coef', 1)
            # seed for random initial phases
            random.seed(kwargs.get('i_seed', 0))
            # initial phase vector
            phi_vec = random.rand(len(self.f[::self.c]))*2*pi
            # propagated initial phases in time using the time vector
            self.phi_t = phi_vec[:, newaxis] + \
                outer(2*pi*self.f[::self.c], self.t)
        else:
            # update the phases from the previous last time
            self.phi_t = self.phi_t[:, -1][:, newaxis] + \
                outer(2*pi*self.f[::self.c], self.t)
        # calculate the complex amplitudes
        A_s = self.elev_map[::self.c][:, newaxis]*exp(-1j*self.phi_t)
        # get back to physical space
        sound = real(A_s.sum(axis=0))
        # rescale to make the signal range approximately between -1 and 1
        m0 = 0.5*(self.elev_map[::self.c]**2).sum()
        sound /= (4*sqrt(m0))
        if not hasattr(self, 'sound'):
            # initiate the sound signal
            self.sound = sound[1:]
        else:
            # update the sound signal and apply a smooth transition between
            # signals to avoid (at least reduce) bumps
            self.sound = self.smooth_transitions(self.sound, sound[1:])

    def smooth_transitions(self, data, data_new, Nx=20):
        '''
        Method that smoothes the transition between two discontinuous data sets
        using a cubic polynomial.

        Inputs
        ----------
        data :: 1darray :: First data set.
        data_new :: 1darray :: Second data set.
        Nx :: int :: Number of points to modify at the beginning of data_new to
                     smooth the transition with the end of data.

        Outputs
        ----------
        data_smooth :: 1darray :: Array similar to data_new except the first Nx
                                  points which have beed adapted to have a
                                  smooth transition with the end of data.
        '''
        # domain to modify
        x = linspace(0., Nx-1, Nx)
        # boundaries of domain
        x1, x2 = 0, Nx-1
        # function values at the domain boundaries
        f1, f2 = data[-1], data_new[Nx-1]
        # gradient values at the domain boundaries
        fp1 = data[-1]-data[-2]
        fp2 = data_new[Nx]-data_new[Nx-1]
        # get the coefficients of cubic the polynomial that satisfies the
        # boundary conditions
        coef = self.calculate_cubic_coef(x1, x2, f1, f2, fp1, fp2)
        # calculate the polynomial values over the modified domain
        f = coef[0]*x**3 + coef[1]*x**2 + coef[2]*x + coef[3]
        # append the polynomial to the rest of the new signal
        data_smooth = append(f, data_new[Nx:])
        return data_smooth

    def calculate_cubic_coef(self, x1, x2, f1, f2, fp1, fp2):
        '''
        Method that calculates the coefficients of a cubic polynomial f with
        the following boundary conditions:
            f(x1)  = f1
            f(x2)  = f2
            f'(x1) = fp1
            f'(x2) = fp2        with f(x) = ax^3 + bx^2 + cx + d

        Inputs
        ----------
        x1 :: float :: Lower domain boundary.
        x2 :: float :: Upper domain boundary.
        f1 :: float :: Function value at the lower domain boundary.
        f2 :: float :: Function value at the upper domain boundary.
        fp1 :: float :: Function gradient at the lower domain boundary.
        fp2 :: float :: Function gradient at the upper domain boundary.

        Outputs
        ----------
        coef :: 1darray :: Array of coefficient (a,b,c,d) of cubic polynomial.
        '''
        # create matrix to invert
        A = array([[x1**3,   x1**2, x1, 1.],
                   [x2**3,   x2**2, x2, 1.],
                   [3*x1**2, 2*x1,  1., 0.],
                   [3*x2**2, 2*x2,  1., 0.]])
        # create RHS vector
        B = array([f1, f2, fp1, fp2])
        # invert matrix and calculate the solution vector
        coef = dot(linalg.inv(A), B)
        return coef

    def elev_from_full_spectrum_3D(self, A, dkx, dky):
        '''
        Method that generates the surface elevation field from the one-sided
        surface elevation spectrum based on the 2DIFFT. Also useful for any
        regular 2DIFFT that fits the formalism.

        Inputs
        ----------
        A :: 2darray :: Wavenumber surface elevation spectrum, array of complex
                        numbers.
        dkx :: float :: Wavenumber step of the spectrum along the x-direction.
        dky :: float :: Wavenumber step of the spectrum along the y-direction.

        Outputs
        -------
        elev :: 2darray :: Regularly sampled real data (elevation).
        space :: 2darrays :: Spatial grids associated with the elevation field.
        '''
        # get the dimensions (NUMPY CONVENTION)
        nx, ny = A.shape[0], A.shape[1]
        # shift the frequencies to match the numpy formalism
        mat = fft.ifftshift(A)
        # perform the IFFT
        elev = real(fft.ifft2(mat, axes=(0, 1), norm=None))*nx*ny
        # calculate the lengths of the output signal
        Lx, Ly = 2*pi/dkx, 2*pi/dky
        # calculate the signal discretizations
        dx, dy = Lx/nx, Lx/ny
        # calculate the spatial grids
        space = meshgrid(linspace(0., Lx-dx, nx),
                         linspace(0., Ly-dy, ny),
                         indexing='ij')
        return space, elev

    def get_kp(self):
        '''
        Method that calculates the peak wavenumber from the peak angular
        frequency and non-dimensional water depth using the linear dispersion
        relation.
        '''

        def func(kp, wp):
            return wp-sqrt(kp*9.81*tanh(self.mu))

        x0 = self.wp**2/9.81
        self.kp = fsolve(func, x0, args=self.wp)[0]

    def jonswap_spectrum_w_sigma(self, w, sigma):
        '''
        Method that calculates the spectral amplitudes from angular frequencies
        following a JONSWAP spectrum formulation.

        Inputs
        ----------
        w :: 1darray :: Angular frequencies.
        sigma :: float :: Spectral shape parameter.

        Outputs
        ----------
        E :: 1darray :: Spectral amplitudes assiciated to the input angular
                        frequencies.
        '''
        # calculate the JONSWAP spectral amplitudes
        E = 9.81**2/w**5*exp(-5./4.*(self.wp/w)**4) * \
            self.gamma**(exp(-(w-self.wp)**2/(2*sigma**2*self.wp**2)))
        return E

    def directional_spreading_cos_no_scaling(self):
        '''
        Method that calculates the directional spreading function from the
        wave directions of propagation according to a cos^{2s} formulation. The
        spreading function is then multiplied to the wavenumber spectrum to
        obtain the directional wave spectrum.

        Outputs
        ----------
        G :: 2darray :: Spreading function.
        '''
        # calculate the cos^{2s} spreading function
        G = abs(cos(0.5*(self.theta-deg2rad(self.theta_dir))))**(2*self.s)
        return G
