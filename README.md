# ocean2sound

Real-time and interactive weakly nonlinear ocean wave field simulations from a JONSWAP spectrum. Another feature creates a continuous sound from ocean surface datasets. Demonstrative video available [here](https://www.youtube.com/watch?v=071wzBhxOx8). If you like this project, you may be interested in [sound2ocean](https://gitlab.com/ndesmars/sound2ocean).

Through an interactive interface, the user can modify in live some wave field and visualization parameters.

Qt and OpenGL python bindings are extensively used through [PyQtGraph](http://www.pyqtgraph.org/).

The project documentation consists of this README file and the [PDF document](https://gitlab.com/ndesmars/ocean2sound/-/blob/master/formulae_of_surface_quantities.pdf) 'formulae_of_surface_quantities'. Also the code is substantially commented, so refer to it for more implementation details and to the references for further theoretical considerations.

## Dependencies

The following Python packages are required:

- [NumPy](https://numpy.org/)
- [SciPy](https://scipy.org/)
- [YAML](https://yaml.org/)
- [PyQtGraph](http://www.pyqtgraph.org/)
- [PyQt5](https://www.riverbankcomputing.com/static/Docs/PyQt5/)
- [PyOpenGL](http://pyopengl.sourceforge.net/)

Install them with pip by running

`pip install numpy scipy PyYAML pyqtgraph PyQt5 PyOpenGL PyOpenGL_accelerate`

Additionally, [PyAudio](http://people.csail.mit.edu/hubert/pyaudio/) (which provides Python bindings for [PortAudio](https://www.portaudio.com/)) is required. On macOS, it can be easily installed with homebrew and pip by running

`brew install portaudio`\
`pip install PyAudio`

For other OS, refer to the [PyAudio installation documentation](http://people.csail.mit.edu/hubert/pyaudio/#downloads).

## Running ocean2sound

To run ocean2sound with the _default_ initial simulation parameters, simply run

`python main.py`

If you want to use _custom_ initial parameters, run

`python main.py initilization_file.yaml`

in which the argument `initilization_file.yaml` is a YAML file containing the initial parameters of the simulation. _All_ the initial parameters described below are required in the initialization file to properly initiate the simulation. Some parameters are fixed and cannot be changed during the simulation, some are editable through the GUI.

Examples of initialization files are provided in the folder `./init_examples`. Screenshots of the corresponding results are shown in the gallery section. For instance, the first example (`init_param_ex1.yaml` ― containing the default initial parameters) corresponds to a realistic wave field.

### Non-live-editable parameters
- `fps` ― float ― Number of frames per second
- `Nk` ― int ― Number of wave components along each direction in the wave model (the total number of wave components is `Nk*Nk`) ― _Note that optimal numerical performances are reached when_ `Nk` _is equal to a power of 2._
- `i_full_screen` ― bool ― Set to `True` to have the GUI in full screen
- `i_multiple` ― bool ― Set to `True` to replicate the domain multiple (`2*2 = 4`) times
- `i_sound` ― bool ― Set to `True` to stream the 'ocean wave' sound signal
- `Nt` ― int ― Number of time steps in each sound subsignal (updated every `1/fps` second)
- `phase_seed` ― int ―  Seed for the initial distribution of the wave phases

### Live-editable wave field parameters
- `Tp` ― float ― Peak wave period in seconds (period of maximal wave energy)
- `gamma` ― float ― Peak-enhancement factor (how much the wave energy is concentrated around `Tp`)
- `eps` ― float ― Characteristic wave steepness (how steep the waves are) ― _Note that the wave model allows the steepness to be arbitrarily high, potentially leading the simulation to be physically unrealistic._
- `theta_dir` ― float ― Main direction of propagation in degrees (to which direction the waves are traveling)
- `s` ― float ― Directional spreading factor (how much the wave energy is concentrated around `theta_dir`)
- `NL` ― float ― Number of peak wavelengths in each direction (spatial scale of the simulation)
- `mu` ― float ― Non-dimentional water depth `mu = kp*depth` with `kp` the peak wavenumber (how dispersive the waves are)

### Live-editable visualization parameters
- `plotting_style` ― string ― Plotting style, in `{mesh, points}`
- `colormap` ― string ― Colormap name, in `{haze_cyan, inferno, magma, PiYG, erdc_blue2cyan_BW, viridis, parula, x_ray, RF, LF, ASJ}` (add `_r` for the reversed version, e.g., `magma_r`)
- `mapped_quantity` ― string ― Quantity mapped by the colormap, in `{elevation, velocity x, velocity y, velocity z, absolute velocity, slope x, slope y, absolute slope}` ― _Note that the elevation and velocities are normalized such that the resulting fields approximately range between -1 and 1. The slopes are not normalized._
- `mds` ― string ― Mesh display style (only used if `plotting_style=mesh`), in `{faces, wireframe}`
- `pds` ― string ― Points display style (only used if `plotting_style=points`), in `{additive, translucent, opaque}`
- `point_size` ― float ― Point size (only used if plot style is points)
- `vmin` ― float ― Minimal colormap value
- `vmax` ― float ― maximal colormap value
- `hue` ― float ― Hue correction of the colormap, in `[0., 1.]`
- `sat` ― float ― Saturation correction of the colormap, in `[-1., 1.]`
- `val` ― float ― Value (lightness) correction of the colormap,  in `[-1., 1.]`
- `fov` ― float ― Field of view of the camera in degrees
- `elevation` ― float ― Elevation angle the camera in degrees
- `azimuth` ― float ― Azimuth angle of the camera in degrees
- `center`― 1darray ― x-, y-, z-coordinates (normalized by the peak wavelength) of the center point of the camera's view
- `distance` ― float ― Distance (normalized by the peak wavelength) of the camera from the center point

## Gallery

A demonstrative video is available [here](https://www.youtube.com/watch?v=071wzBhxOx8). Below is a selection of screenshots from simulations using the provided examples of initialization files.

Using the initial parameters from `./init_examples/init_param_ex1.yaml`:

<img src="gallery/example1.png" alt="example1" width="800"/>

Using the initial parameters from `./init_examples/init_param_ex2.yaml`:

<img src="gallery/example2.png" alt="example2" width="800"/>

Using the initial parameters from `./init_examples/init_param_ex3.yaml`:

<img src="gallery/example3.png" alt="example3" width="800"/>

Using the initial parameters from `./init_examples/init_param_ex4.yaml` **(the sound stream is active for this one)**:

<img src="gallery/example4.png" alt="example4" width="800"/>

Using the initial parameters from `./init_examples/init_param_ex5.yaml`:

<img src="gallery/example5.png" alt="example5" width="800"/>

Using the initial parameters from `./init_examples/init_param_ex6.yaml`:

<img src="gallery/example6.png" alt="example6" width="800"/>

Using the initial parameters from `./init_examples/init_param_ex7.yaml`:

<img src="gallery/example7.png" alt="example7" width="800"/>

## References

The ocean wave spectrum follows the so-called JONSWAP formulation:

Hasselmann, K., Barnet, T., Bouws, E., Carlson, H., Cartwright, D., Enke, K., Ewing, J., Gienapp, H., Hasselmann, D., Krusemann, P., Meerburg, A., Müller, P., Olbers, D., Richter, K., Seil, W. & Walden, H. (1973) Measurements of wind-wave growth and swell decay during the JOint North Sea WAve Project (JONSWAP). _Deutsche Hydrographische Zeitschrift_, Reihe A 8 (12), 1–95. [[PDF]](https://repository.tudelft.nl/islandora/object/uuid:f204e188-13b9-49d8-a6dc-4fb7c20562fc/datastream/OBJ)

The ocean surface is modeled according to the Improved Choppy Wave Model:

Guérin, C.-A., Desmars, N., Grilli, S. T., Ducrozet, G., Perignon, Y. & Ferrant, P. (2019) An improved Lagrangian model for the time evolution of nonlinear surface waves. _Journal of Fluid Mechanics_ 876, 527–552. [[PDF]](https://hal.archives-ouvertes.fr/hal-02310305/document)

## License

Copyright 2024 Nicolas Desmars

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
