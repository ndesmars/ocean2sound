# -*- coding: utf-8 -*-
'''
Real-time and interactive weakly nonlinear ocean wave field simulations from a
JONSWAP spectrum. Another feature creates a continuous sound from ocean surface
datasets.

Through an interactive GUI, the user can modify in live some wave spectrum and
visualization parameters.

Qt and OpenGL python bindings are extensively used through PyQtGraph.

For more details than those found in the documentation, code comments and
references, please let me know at:
    nicolas _dot_ desmars _at_ protonmail _dot_ com

License
----------
Copyright 2024 Nicolas Desmars

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

TODO
----------
In order to have a smooth sound stream, the beatrate has to be reduced compared
to the expected one fps*(Nt-1). This leads the update of waves to be slower
than real time. Improvements of the sound stream generation should address this
issue.
'''

import yaml
from OceanSurface import OceanSurface
from InteractiveApp import InteractiveApp
import PyQt5
from pyqtgraph.Qt import QtCore
import pyaudio
import sys

if len(sys.argv) == 2:
    # if an initialization file is given as argument
    # get initial parameters from specified initialization file
    init_param_path = sys.argv[1]
    with open(init_param_path, 'r') as yamlFile:
        parsed_init_param = yaml.full_load(yamlFile)
    fps = parsed_init_param['fps']
    Nk = parsed_init_param['Nk']
    i_full_screen = parsed_init_param['i_full_screen']
    i_multiple = parsed_init_param['i_multiple']
    i_sound = parsed_init_param['i_sound']
    Nt = parsed_init_param['Nt']
    phase_seed = parsed_init_param['phase_seed']
    Tp = parsed_init_param['Tp']
    gamma = parsed_init_param['gamma']
    eps = parsed_init_param['eps']
    theta_dir = parsed_init_param['theta_dir']
    s = parsed_init_param['s']
    NL = parsed_init_param['NL']
    mu = parsed_init_param['mu']
    plotting_style = parsed_init_param['plotting_style']
    mds = parsed_init_param['mds']
    pds = parsed_init_param['pds']
    point_size = parsed_init_param['point_size']
    mapped_quantity = parsed_init_param['mapped_quantity']
    colormap = parsed_init_param['colormap']
    hue = parsed_init_param['hue']
    sat = parsed_init_param['sat']
    val = parsed_init_param['val']
    vmin = parsed_init_param['vmin']
    vmax = parsed_init_param['vmax']
    fov = parsed_init_param['fov']
    elevation = parsed_init_param['elevation']
    azimuth = parsed_init_param['azimuth']
    center = parsed_init_param['center']
    distance = parsed_init_param['distance']
    # create an instance of wave field simulation
    OS = OceanSurface(Tp=Tp, gamma=gamma, eps=eps, theta_dir=theta_dir, s=s,
                      NL=NL, mu=mu, phase_seed=phase_seed, Nk=Nk, Nt=Nt)
    # create a Qt application
    app = PyQt5.QtWidgets.QApplication([])
    # create widget of custom GUI
    IA = InteractiveApp(OS, fps=fps, i_multiple=i_multiple, i_sound=i_sound,
                        plotting_style=plotting_style, mds=mds, pds=pds,
                        point_size=point_size, mapped_quantity=mapped_quantity,
                        colormap=colormap, hue=hue, sat=sat, val=val,
                        vmin=vmin, vmax=vmax, fov=fov, elevation=elevation,
                        azimuth=azimuth, center=center, distance=distance)
elif len(sys.argv) == 1:
    # if no initialization file is given
    # set default values for i_full_screen and i_sound
    i_full_screen, i_sound = False, False
    # create an instance of wave field simulation
    OS = OceanSurface()
    # create a Qt application
    app = PyQt5.QtWidgets.QApplication([])
    # create widget of custom GUI
    IA = InteractiveApp(OS)
else:
    # exit if more than two arguments are given
    sys.exit('Please give one valid initialization file as argument, or no ' +
             'argument at all.')
# show the GUI
IA.show()
if i_full_screen:
    # to display the GUI in full screen
    IA.showFullScreen()
if i_sound:
    # initiate the sound stream
    # create a pyaudio instance
    p = pyaudio.PyAudio()
    # open a stream
    stream = p.open(format=pyaudio.paInt16,
                    channels=1,
                    rate=int(IA.fps*(IA.OS.Nt-1)*0.5),  # factor 0.5...!!
                    frames_per_buffer=IA.OS.Nt-1,
                    output=True,
                    stream_callback=IA.stream_callback)
    # start the stream
    stream.start_stream()
# create Qt timer
t = QtCore.QTimer()
# connect it to the time-update function of GUI
t.timeout.connect(IA.update_time)
# calculate refresh rate of the image
image_refresh_rate = int(1/IA.fps*1000)
# start the time updates
t.start(image_refresh_rate)
if __name__ == '__main__':
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        PyQt5.QtWidgets.QApplication.instance().exec_()
